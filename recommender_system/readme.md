
### A simple recommendation system that uses Spark Alternating Least Square library - a matrix factorization approach.

     In this case study, a movie recommendation ran 
     on [Databricks Spark](https://community.cloud.databricks.com)

     Test result was as shown
