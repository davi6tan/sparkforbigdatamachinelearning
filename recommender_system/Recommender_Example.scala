import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.recommendation.ALS

val ratings = spark.read.option("header","true").option("inferSchema","true")
.csv("/FileStore/tables/hivl15uf1480488667173/movie_ratings.csv")


ratings.head()
ratings.printSchema()

val Array(training, test) = ratings.randomSplit(Array(0.8, 0.2))

// Build the recommendation model using ALS on the training data
val als = new ALS()
  .setMaxIter(5)
  .setRegParam(0.01)
  .setUserCol("userId")
  .setItemCol("movieId")
  .setRatingCol("rating")
val model = als.fit(training)

// Evaluate the model by computing the average error from real rating
val predictions = model.transform(test)

// import to use abs()
import org.apache.spark.sql.functions._
val error = predictions.select(abs($"rating"-$"prediction"))

// Drop NaNs
error.na.drop().describe().show()

/*
(1) Spark Jobs
+-------+--------------------------+
|summary|abs((rating - prediction))|
+-------+--------------------------+
|  count|                     19239|
|   mean|        0.8411533425413132|
| stddev|        0.7371093350072391|
|    min|      5.245208740234375E-6|
|    max|        14.979504585266113|
+-------+--------------------------+

Command took 6.57 seconds -- by davidtan at 1/20/2017, 3:31:10 PM on myCluster
 */