import org.apache.spark.sql.SparkSession

val spark = SparkSession.builder.getOrCreate()

val df = spark.read.option("header","true").option("inferSchema","true").csv("Netflix_2011_2016.csv")

df.columns
df.printSchema()

df.head(5)

df.describe().show()

val df2 = df.withColumn("HV Ratio",df("High")/df("Volume"))

df.orderBy($"High".desc).show(1)

df.select(mean("Close")).show()

df.select(max("Volume")).show()
df.select(min("Volume")).show()
//////

import spark.implicits._

df.filter($"Close"<600).count()
(df.filter($"High">500).count()*1.0/df.count())*100
/// find Pearson corelllation between High and Volume?

df.select(corr("High","Volume")).show()


val yeardf = df.withColumn("Year",year(df("Date")))

val yearmaxs = yeardf.select($"Year",$"High").groupBy("Year").max()
yearavgs.select($"Year",$"max(High)").show()


////
val monthdf = df.withColumn("Month",month(df("Date")))

val monthavgs = monthdf.select($"Month",$"Close").groupBy("Month").mean()
monthavgs.select($"Month",$("avg(Close)")).show()

