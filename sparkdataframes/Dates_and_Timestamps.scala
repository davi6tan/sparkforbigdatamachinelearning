// Dates and TimeStamps

// Start a simple Spark Session
import org.apache.spark.sql.SparkSession
val spark = SparkSession.builder().getOrCreate()

// Create a DataFrame from Spark Session read csv
// Technically known as class Dataset
val df = spark.read.option("header","true").option("inferSchema","true").csv("CitiGroup2006_2008")

// Show Schema
df.printSchema()

// Lot's of options here
// http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.functions$@add_months(startDate:org.apache.spark.sql.Column,numMonths:Int):org.apache.spark.sql.Column

df.select(month(df("Date"))).show()

df.select(year(df("Date"))).show()

// Practical Example
val df2 = df.withColumn("Year",year(df("Date")))

// Mean per Year, notice large 2008 drop!
val dfavgs = df2.groupBy("Year").mean()
dfavgs.select($"Year",$"avg(Close)").show()

/*
scala> :load Dates_and_Timestamps.scala
Loading Dates_and_Timestamps.scala...
import org.apache.spark.sql.SparkSession
spark: org.apache.spark.sql.SparkSession = org.apache.spark.sql.SparkSession@609f136b
df: org.apache.spark.sql.DataFrame = [Date: timestamp, Open: double ... 4 more fields]
root
 |-- Date: timestamp (nullable = true)
 |-- Open: double (nullable = true)
 |-- High: double (nullable = true)
 |-- Low: double (nullable = true)
 |-- Close: double (nullable = true)
 |-- Volume: integer (nullable = true)

+-----------+
|month(Date)|
+-----------+
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
|          1|
+-----------+
only showing top 20 rows

+----------+
|year(Date)|
+----------+
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
|      2006|
+----------+
only showing top 20 rows

df2: org.apache.spark.sql.DataFrame = [Date: timestamp, Open: double ... 5 more fields]
dfavgs: org.apache.spark.sql.DataFrame = [Year: int, avg(Open): double ... 5 more fields]
+----+------------------+
|Year|        avg(Close)|
+----+------------------+
|2007| 477.8203984063745|
|2006| 489.2697211155379|
|2008|190.48893280632404|
+----+------------------+

 */
