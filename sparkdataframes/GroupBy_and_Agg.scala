// GROUP BY and AGG (Aggregate methods)

// Start a simple Spark Session
import org.apache.spark.sql.SparkSession
val spark = SparkSession.builder().getOrCreate()

// Create a DataFrame from Spark Session read csv
// Technically known as class Dataset
val df = spark.read.option("header","true").option("inferSchema","true").csv("Sales.csv")

// Show Schema
df.printSchema()

// Show
df.show()

// Groupby Categorical Columns
// Optional, usually won't save to another object
df.groupBy("Company")

// Mean
df.groupBy("Company").mean().show()
// Count
df.groupBy("Company").count().show()
// Max
df.groupBy("Company").max().show()
// Min
df.groupBy("Company").min().show()
// Sum
df.groupBy("Company").sum().show()

// Other Aggregate Functions
// http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.functions$
df.select(countDistinct("Sales")).show() //approxCountDistinct
df.select(sumDistinct("Sales")).show()
df.select(variance("Sales")).show()
df.select(stddev("Sales")).show() //avg,max,min,sum,stddev
df.select(collect_set("Sales")).show()

// OrderBy
// Ascending
df.orderBy("Sales").show()

// Descending
df.orderBy($"Sales".desc).show()

/*
scala> :loaf GroupBy_and_Agg.scala
loaf: no such command.  Type :help for help.

scala> :load GroupBy_and_Agg.scala
Loading GroupBy_and_Agg.scala...
import org.apache.spark.sql.SparkSession
spark: org.apache.spark.sql.SparkSession = org.apache.spark.sql.SparkSession@609f136b
df: org.apache.spark.sql.DataFrame = [Company: string, Person: string ... 1 more field]
root
 |-- Company: string (nullable = true)
 |-- Person: string (nullable = true)
 |-- Sales: integer (nullable = true)

+-------+-------+-----+
|Company| Person|Sales|
+-------+-------+-----+
|   GOOG|    Sam|  200|
|   GOOG|Charlie|  120|
|   GOOG|  Frank|  340|
|   MSFT|   Tina|  600|
|   MSFT|    Amy|  124|
|   MSFT|Vanessa|  243|
|     FB|   Carl|  870|
|     FB|  Sarah|  350|
+-------+-------+-----+

res50: org.apache.spark.sql.RelationalGroupedDataset = org.apache.spark.sql.RelationalGroupedDataset@5344166e
+-------+-----------------+
|Company|       avg(Sales)|
+-------+-----------------+
|   GOOG|            220.0|
|     FB|            610.0|
|   MSFT|322.3333333333333|
+-------+-----------------+

+-------+-----+
|Company|count|
+-------+-----+
|   GOOG|    3|
|     FB|    2|
|   MSFT|    3|
+-------+-----+

+-------+----------+
|Company|max(Sales)|
+-------+----------+
|   GOOG|       340|
|     FB|       870|
|   MSFT|       600|
+-------+----------+

+-------+----------+
|Company|min(Sales)|
+-------+----------+
|   GOOG|       120|
|     FB|       350|
|   MSFT|       124|
+-------+----------+

+-------+----------+
|Company|sum(Sales)|
+-------+----------+
|   GOOG|       660|
|     FB|      1220|
|   MSFT|       967|
+-------+----------+

+---------------------+
|count(DISTINCT Sales)|
+---------------------+
|                    8|
+---------------------+

+-------------------+
|sum(DISTINCT Sales)|
+-------------------+
|               2847|
+-------------------+

+-----------------+
|  var_samp(Sales)|
+-----------------+
|67235.55357142855|
+-----------------+

+------------------+
|stddev_samp(Sales)|
+------------------+
|259.29819430807567|
+------------------+

+--------------------+
|  collect_set(Sales)|
+--------------------+
|[350, 340, 870, 1...|
+--------------------+

+-------+-------+-----+
|Company| Person|Sales|
+-------+-------+-----+
|   GOOG|Charlie|  120|
|   MSFT|    Amy|  124|
|   GOOG|    Sam|  200|
|   MSFT|Vanessa|  243|
|   GOOG|  Frank|  340|
|     FB|  Sarah|  350|
|   MSFT|   Tina|  600|
|     FB|   Carl|  870|
+-------+-------+-----+

+-------+-------+-----+
|Company| Person|Sales|
+-------+-------+-----+
|     FB|   Carl|  870|
|   MSFT|   Tina|  600|
|     FB|  Sarah|  350|
|   GOOG|  Frank|  340|
|   MSFT|Vanessa|  243|
|   GOOG|    Sam|  200|
|   MSFT|    Amy|  124|
|   GOOG|Charlie|  120|
+-------+-------+-----+


 */