// DataFrame Operations

// Start a simple Spark Session
import org.apache.spark.sql.SparkSession
val spark = SparkSession.builder().getOrCreate()

// Create a DataFrame from Spark Session read csv
// Technically known as class Dataset
val df = spark.read.option("header","true").option("inferSchema","true").csv("CitiGroup2006_2008")

// Show Schema
df.printSchema()

// Show head
// df.head(5)

//////////////////////////
//// FILTERING DATA //////
//////////////////////////
// This import is needed to use the $-notation
import spark.implicits._

// Grabbing all rows where a column meets a condition
df.filter($"Close" > 480).show()

// Can also use SQL notation
//df.filter("Close > 480").show()

// Count how many results
df.filter($"Close">480).count()

// Can also use SQL notation
// df.filter("Close > 480").count()

// Multiple Filters
// Note the use of triple === , this may change in the future!
df.filter($"High"===484.40).show()
// Can also use SQL notation
// df.filter("High = 484.40").count()

df.filter($"Close"<480 && $"High"<480).show()
// Can also use SQL notation
// df.filter("Close<480 AND High < 484.40").show()

// Collect results into a scala object (Array)
val High484 = df.filter($"High"===484.40).collect()

// Operations and Useful Functions
// http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.functions$

// Examples of Operations
df.select(corr("High","Low")).show() // Pearson Correlation


/*
scala> :load DataFrame_Operations.scala
Loading DataFrame_Operations.scala...
import org.apache.spark.sql.SparkSession
spark: org.apache.spark.sql.SparkSession = org.apache.spark.sql.SparkSession@609f136b
df: org.apache.spark.sql.DataFrame = [Date: timestamp, Open: double ... 4 more fields]
root
 |-- Date: timestamp (nullable = true)
 |-- Open: double (nullable = true)
 |-- High: double (nullable = true)
 |-- Low: double (nullable = true)
 |-- Close: double (nullable = true)
 |-- Volume: integer (nullable = true)

import spark.implicits._
+--------------------+-----+-----+-----+-----+-------+
|                Date| Open| High|  Low|Close| Volume|
+--------------------+-----+-----+-----+-----+-------+
|2006-01-03 00:00:...|490.0|493.8|481.1|492.9|1537660|
|2006-01-04 00:00:...|488.6|491.0|483.5|483.8|1871020|
|2006-01-05 00:00:...|484.4|487.8|484.0|486.2|1143160|
|2006-01-06 00:00:...|488.8|489.0|482.0|486.2|1370250|
|2006-01-09 00:00:...|486.0|487.4|483.0|483.9|1680740|
|2006-01-10 00:00:...|483.0|485.5|480.8|485.4|1365960|
|2006-01-11 00:00:...|495.8|495.8|485.8|489.8|1684440|
|2006-01-12 00:00:...|491.0|491.0|488.8|490.3|1230060|
|2006-01-13 00:00:...|491.0|491.9|487.3|489.2| 940930|
|2006-01-17 00:00:...|485.1|487.0|482.7|484.3|1237830|
|2006-01-18 00:00:...|484.3|486.7|481.1|483.8|1218910|
|2006-04-04 00:00:...|474.3|483.8|474.1|482.1|1612390|
|2006-04-05 00:00:...|483.0|484.7|481.3|482.6|1276340|
|2006-04-13 00:00:...|474.1|481.8|474.1|480.5|1197840|
|2006-04-17 00:00:...|484.1|485.6|482.0|483.5|1678120|
|2006-04-18 00:00:...|484.4|486.0|479.0|484.8|1803670|
|2006-04-19 00:00:...|484.8|487.1|480.5|482.3|1445480|
|2006-04-20 00:00:...|483.0|487.2|482.6|483.0|1866460|
|2006-04-21 00:00:...|484.9|484.9|477.9|480.1|1579130|
|2006-04-27 00:00:...|472.0|484.4|471.5|481.5|2464800|
+--------------------+-----+-----+-----+-----+-------+
only showing top 20 rows

res15: Long = 325
+--------------------+-----+-----+-----+-----+-------+
|                Date| Open| High|  Low|Close| Volume|
+--------------------+-----+-----+-----+-----+-------+
|2006-04-27 00:00:...|472.0|484.4|471.5|481.5|2464800|
+--------------------+-----+-----+-----+-----+-------+

+--------------------+-----+-----+-----+-----+-------+
|                Date| Open| High|  Low|Close| Volume|
+--------------------+-----+-----+-----+-----+-------+
|2006-01-20 00:00:...|472.1|474.0|456.3|456.9|4781930|
|2006-01-23 00:00:...|460.0|463.8|457.0|460.0|2025500|
|2006-01-24 00:00:...|462.9|463.6|459.9|460.1|2083740|
|2006-01-25 00:00:...|461.4|463.7|460.1|462.3|1591940|
|2006-01-26 00:00:...|465.5|475.5|464.5|470.1|1988600|
|2006-01-27 00:00:...|470.1|473.7|466.0|468.7|1412760|
|2006-01-30 00:00:...|468.7|469.9|466.6|468.2|1057630|
|2006-01-31 00:00:...|468.3|470.5|465.5|465.8|1887280|
|2006-02-01 00:00:...|465.9|467.2|461.1|463.3|1844970|
|2006-02-02 00:00:...|459.0|461.0|451.0|451.8|2325470|
|2006-02-03 00:00:...|450.7|456.1|448.1|450.6|1666510|
|2006-02-06 00:00:...|452.6|456.1|450.9|451.7|1147430|
|2006-02-07 00:00:...|452.0|453.8|450.0|450.5|1207780|
|2006-02-08 00:00:...|453.3|455.3|450.7|453.6|1051370|
|2006-02-09 00:00:...|455.0|461.0|454.3|457.9|1357740|
|2006-02-10 00:00:...|457.0|460.7|452.5|459.6|1272030|
|2006-02-13 00:00:...|460.6|462.3|454.1|456.8|1158300|
|2006-02-14 00:00:...|457.8|462.5|457.1|461.2|1518040|
|2006-02-15 00:00:...|460.4|464.7|457.6|462.5|1700050|
|2006-02-16 00:00:...|463.0|464.4|460.4|464.4|1326000|
+--------------------+-----+-----+-----+-----+-------+
only showing top 20 rows

High484: Array[org.apache.spark.sql.Row] = Array([2006-04-27 00:00:00.0,472.0,484.4,471.5,481.5,2464800])
+------------------+
|   corr(High, Low)|
+------------------+
|0.9992999172726325|
+------------------+


 */