// Start a simple Spark Session
import org.apache.spark.sql.SparkSession
val spark = SparkSession.builder().getOrCreate()

// Grab small dataset with some missing data
val df = spark.read.option("header","true").option("inferSchema","true").csv("ContainsNull.csv")

// Show schema
df.printSchema()

// Notice the missing values!
df.show()

// You basically have 3 options with Null values
// 1. Just keep them, maybe only let a certain percentage through
// 2. Drop them
// 3. Fill them in with some other value
// No "correct" answer, you'll have to adjust to the data!

// Dropping values
// Technically still experimental, but it has been around since 1.3

// Drop any rows with any amount of na values
df.na.drop().show()

// Drop any rows that have less than a minimum Number
// of NON-null values ( < Int)
df.na.drop(2).show()

// Interesting behavior!
// What happens when using double/int versus strings

// Fill in the Na values with Int
df.na.fill(100).show()

// Fill in String will only go to all string columns
df.na.fill("Emp Name Missing").show()

// Be more specific, pass an array of string column names
df.na.fill("Specific",Array("Name")).show()

// There are also more complicated map capabilities!

// Exercise: Fill in Sales with average sales.
// How to get averages? For now, a simple way is to use describe!
df.describe().show()

// Now fill in with the values
df.na.fill(400.5).show()

// It would be nice to be able to just directly
// use aggregate functions. So let's learn about them
// with groupby and agg up next!
/*
scala> :load MissingData.scala
Loading MissingData.scala...
import org.apache.spark.sql.SparkSession
spark: org.apache.spark.sql.SparkSession = org.apache.spark.sql.SparkSession@609f136b
df: org.apache.spark.sql.DataFrame = [Id: string, Name: string ... 1 more field]
root
 |-- Id: string (nullable = true)
 |-- Name: string (nullable = true)
 |-- Sales: double (nullable = true)

+----+-----+-----+
|  Id| Name|Sales|
+----+-----+-----+
|emp1| John| null|
|emp2| null| null|
|emp3| null|345.0|
|emp4|Cindy|456.0|
+----+-----+-----+

+----+-----+-----+
|  Id| Name|Sales|
+----+-----+-----+
|emp4|Cindy|456.0|
+----+-----+-----+

+----+-----+-----+
|  Id| Name|Sales|
+----+-----+-----+
|emp1| John| null|
|emp3| null|345.0|
|emp4|Cindy|456.0|
+----+-----+-----+

+----+-----+-----+
|  Id| Name|Sales|
+----+-----+-----+
|emp1| John|100.0|
|emp2| null|100.0|
|emp3| null|345.0|
|emp4|Cindy|456.0|
+----+-----+-----+

+----+----------------+-----+
|  Id|            Name|Sales|
+----+----------------+-----+
|emp1|            John| null|
|emp2|Emp Name Missing| null|
|emp3|Emp Name Missing|345.0|
|emp4|           Cindy|456.0|
+----+----------------+-----+

+----+--------+-----+
|  Id|    Name|Sales|
+----+--------+-----+
|emp1|    John| null|
|emp2|Specific| null|
|emp3|Specific|345.0|
|emp4|   Cindy|456.0|
+----+--------+-----+

+-------+----+-----+-----------------+
|summary|  Id| Name|            Sales|
+-------+----+-----+-----------------+
|  count|   4|    2|                2|
|   mean|null| null|            400.5|
| stddev|null| null|78.48885271170677|
|    min|emp1|Cindy|            345.0|
|    max|emp4| John|            456.0|
+-------+----+-----+-----------------+

+----+-----+-----+
|  Id| Name|Sales|
+----+-----+-----+
|emp1| John|400.5|
|emp2| null|400.5|
|emp3| null|345.0|
|emp4|Cindy|456.0|
+----+-----+-----+


 */