import org.apache.spark.sql.SparkSession

val spark = SparkSession.builder.getOrCreate()

val df = spark.read.option("header","true").option("inferSchema","true").csv("CitiGroup2006_2008")

df.head(5)
println("\n")

for(line <- df.head(10)){
  println(line)
}

df.columns


df.printSchema()

df.describe()


df.select("Volume").show()


df.select($"Date",$"Close").show(2)


val df2 = df.withColumn("HighPlusLow",df("High")-df("Low"))

df2.columns

df2.printSchema()

df2.head(5)

df2.select(df2("HighPlusLow").as("HPL"),df2("Close")).show()
