# Apache Spark in Scala for Big Data and Machine Learning



### Highlights

- Spark Data Frames

- Machine Learning - Regression 

- Machine Learning - Recommender system


*Adapted from Jose Portilla* 


Based on 

- [Machine Learning Guide](http://spark.apache.org/docs/latest/ml-guide.html)
- [Apache Spark Api](http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.package)